DROP DATABASE IF EXISTS lab;
CREATE DATABASE lab;
\c lab

CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
CREATE TABLE Inventory (
    username TEXT,
    product TEXT,
    amount INT CHECK(amount >= 0 AND amount <= 100),
    CONSTRAINT uniq PRIMARY KEY (username, product)
);

INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO PLayer (username, balance) VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
INSERT INTO Shop (product, in_stock, price) VALUES ('air', 1000000, 1);
