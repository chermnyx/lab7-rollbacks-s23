# Lab 7

## Lab

### `buy_product.py` result

![](./screenshots/db_script.png)

### DB State after the script

![](./screenshots/inventory.png)
![](./screenshots/player.png)
![](./screenshots/shop.png)

## Extra part

### 1.

In this case we can add additional check for the completed transactions and check the completion on each request for example, by generating one time token to make a transaction or assigning each user requested transaction an ID and invalidating the request if the transactions with the requested id is marked as completed.

### 2.

Here are the possible solutions:

1. Use a distributed transaction manager that can coordinate transactions across multiple storages: either all of the transactions will be committed or all of them will be rolled back as a single big transaction.
2. Use a carefully designed compensating transactions as a workaround for failed transactions: check which of the transactions are failed and ensure that the compensating transaction is going to be executed.
3. Use two-phase commit protocol: first the transactions are marked as "prepared" and only when all of the transactions are marked as "prepared" the transactions are committed and the big transaction itself is commited.
4. Log the transactions in a journal and rollback partially commited transactions.
