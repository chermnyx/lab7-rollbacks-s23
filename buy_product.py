import psycopg2
import psycopg2.errors


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="postgres",
        host="127.0.0.1",
        port=5432
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    print('buy', obj)
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute('BEGIN')
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(
                    'SELECT SUM(amount) FROM Inventory WHERE username = %(username)s', obj)

                # this is not needed as the final amount is checked inside the DBMS by the specified constraint
                # inv = cur.fetchone()
                # if inv is not None:
                #     inv = inv[0]
                #     if inv + amount > 100:
                #         cur.execute('ROLLBACK')
                #         raise Exception("Too many items")

                cur.execute('INSERT INTO Inventory (username, product, amount) \
                                VALUES (%(username)s, %(product)s, %(amount)s) \
                             ON CONFLICT (username, product) \
                                DO UPDATE SET amount = Inventory.amount + %(amount)s', obj)

                cur.execute('COMMIT')

            except psycopg2.errors.CheckViolation:
                cur.execute('ROLLBACK')
                print('check violation')
                raise

            except psycopg2.errors.UniqueViolation:
                # ignore as the transaction is committed with updated amount by "ON CONFLICT" part of the query
                print('unique violation')
                pass

            except Exception:
                raise

buy_product('Alice', 'marshmello', 1)

try:
    buy_product('Bob', 'air', 99)
    buy_product('Bob', 'air', 1)

    # should fail here
    buy_product('Bob', 'air', 1)
except:
    pass

try:
    # should fail; no money for Alice
    buy_product('Alice', 'marshmello', 99)
except:
    pass
